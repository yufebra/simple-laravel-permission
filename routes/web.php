<?php

use App\Http\Controllers\Auth\AdminRegisterController;
use App\Http\Controllers\DataController;
use App\Http\Middleware\AdminAccessOnlyMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Mengecek semua daftar route bisa dengan command berikut di cmd
// php artisan route:list

Route::get('/', function () {
    return view('welcome');
});

// Bawaan laravel ui untuk mendaftarkan route untuk registrasi dan login
Auth::routes();

//Group berarti mengelompokkan semua route didalam callback (fungsi lambda/anonymus)
Route::middleware(AdminAccessOnlyMiddleware::class)->group(function (){
    //Route::<jenis request get/post/put/delete>(nama url, [nama file controller, fungsi didalam file yg dipanggil])->name('nama route')
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});
// 3 baris diatas itu sama dengan
// Route::middleware(AdminAccessOnlyMiddleware::class)->get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Apabila controller dibuat dengan resource (-r), daftar route akan didefine otomatis dengan code berikut
// Route::resource(awalan nama route, class yang dipanggil);
Route::resource('data', DataController::class);

// Pengalamanku pribadi, semisal anda pengen membuat nama route yang seperti mempunyai sub (Misal: home.sub1). Itu mending urlnya define sendiri aja jangan pake Route::resource
// Karena nanti mesti ditanyai parameter tambahan home yang mana. Contoh mau manggil show dengan misal diatas. Urlnya adalah /home/{home}/sub1/{id}

// Jadi buatnya seperti ini, semua parameternya sama seperti get()
// Route::get => untuk fungsi index(), create(), show(), edit()
// Route::post => untuk store()
// Route::put => untuk update()
// Route::delete => untuk delete()
