@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <!-- Baris yang dicepit if hanya bisa diakses ketika user memiliki permission adm -->
                <!-- FYI: auth()->user() itu bisa memanggil semua kolom dari baris user yang sedang login. -->
                <!-- Misal "auth()->user()->name" itu berarti mengambil nama user yang sedang login-->
                @if(auth()->user()->permission == "adm")
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
