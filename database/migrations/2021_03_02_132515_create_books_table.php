<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            // ForeignId digunakan untuk mempersingkat membuat relasi tabel.
            // $table->foreignId(namaKolom)->constrained(tabelYangBerelasi);
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');

            //Itu sama saja dengan 2 baris code berikut
            // $table->bigInteger('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        // Tips lagi
        // Dalam membuat relasi itu pastikan tambahi ->onDelete(); Buat memberi aksi tambahan ketika data parent dihapus
        // ->onDelete('cascade') itu menghapus data child/baris di tabel ini juga apabila data parent dihapus
        // ->onDelete('set null') itu mengubah kolom ini mejadi null apabila data parent dihapus. Tapi ingat setelah define nama kolom berikan ->nullable() yang maksudnya kolom ini boleh null
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
