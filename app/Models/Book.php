<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// Tips mempercepat membuat tabel.
// Yang dibuat itu modelnya. Yg lain di define dengan property command
// php artisan make:model <nama model dalam singular> -mcsr
// -m = Membuat migration tabel bernama sama dengan model
// -c = Membuat controller bernama sama dengan model
// -s = Membuat seeder untuk tabel ini
// -r = Membuat controller dengan fungsi resource bawaan
// Tapi nk saranku pake -m aja si, soale kadang ada beberapa model ga butuh controller dan seeder
class Book extends Model
{
    use HasFactory;
    protected $fillable = ['user_id'];

    // Baris dibawah ini seumpama anda menghapus $table->timestamps(); di migrationnya. Jadi eloquent tidak akan mengisinya.
    // public $timestamp = false;

    // Apabila di migrationnya dihapus namun di model tidak diberi baris diatas, maka akan terjadi error karena eloquent tidak bisa mengisi.
    // Seperti ketika ada kolom yg tidak di daftarkan di fillable namun user ingin mengisinya menggunakan create.

    // Mengambil user yang memiliki buku X. Karena 1 buku cuman bisa dimiliki 1 user, maka belongsTo
    public function user(){
        // belongsTo(nama file model yg direlasi, nama foreign key di tabel ini, nama primary key tabel yg direlasi)
        // Parameter ke-2 bisa dikosongi apabila nama foreign key di tabel ini berakhiran dengan "id"
        // Parameter ke-3 bisa dikosongi apabila nama primary key tabel relasi asal adalah "id"
        return $this->belongsTo(User::class);
    }
}
