<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'permission'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Mengambil relasi dari book. Karena 1 user bisa memiliki banyak buku. Maka hasMany()
    // Sebenarnya penamaannya terserah si. Tapi aku kebiasaan singular. Jadi nanti manggilnya gini
    // $user = User::with(['book'])->get()
    // $user->book;
    public function book(){
        // hasMany(nama file model yg direlasi, nama foreign key di tabel yg berelasi, nama primary key tabel ini)
        // Parameter ke-2 bisa dikosongi apabila nama foreign key di tabel tujuan berakhiran dengan "id"
        // Parameter ke-3 bisa dikosongi apabila nama primary key tabel ini adalah "id"
        return $this->hasMany(Book::class);

        // hasMany memiliki parameter-parameter yg sama dengan hasOne. Namun hasOne apabila relasinya 1:1
    }

    // Bedanya hasOne dan hasMany?
    // Dari contoh code $user->book
    // Jika bentuknya hasOne => maka jika di dd() yg muncul adalah langsung model yg berelasi (langsung data bukunya)
    // Jika bentuknya hasMany => maka jika di dd() yg muncul adalah array dari semua model yg punya relasi dengan $user (semua buku milik user)
}
