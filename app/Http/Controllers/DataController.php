<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::get();

        // dd itu fungsi bawaan laravel untuk menjelaskan data yang dimasukkan dalam parameter
        // dd($user);

        // return ini akan membuat $user dikirim sebagai json
        return response()->json($user);

        // return ini akan memanggil fungsi show() menggunakan route dengan parameter yg dibutuhkan show
        return redirect()->route('data.show', 'dataYangDikirimDiParameter');

        // Atau bisa mendefine parameter mana yg mau diisi dengan cara berikut
        return redirect()->route('data.show', ['id' => 'dataYangDikirimDiParameter']);
        /// Berati dia mengisi parameter id, baris kyk gitu berguna nk parameter yg dikirim banyak misal seperti fungsi controller berikut
        // public function fungsi($param1, $param2)
        /// Di routenya
        // Route::get('/data/{param1}/get/{param2}, [NamaController:class, 'fungsi'])->name('namaRoute');
        /// Jadi manggilmu kayak gini
        // return redirect()->route('namaRoute', ['param1' => 'data1', 'param2' => 'data2']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($data = "Default Value")
    {
        // FYI tambahan, parameter $data disini mempunyai default value... Jadi seumpama $data itu tidak diisi argumen karena
        // user cuman manggil nama routenya aja tanpa ngasih argumen, maka error tidak terjadi melainkan $data keiisi dengan data tersebut
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Kalau disini $id itu wajib diisi, seumpama ngga diisi maka akan muncul error. Tidak seperti di class create()
        echo "Halo " .$id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
