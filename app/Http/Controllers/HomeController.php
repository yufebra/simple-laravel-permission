<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Ini maksudnya constructor... Semua fungsi yang dipanggil dari class harus memenuhi command" disini, bisa dimanfaatkan untuk memakai middleware seperti ini.
        $this->middleware('auth');
        // Btw darimana 'auth' itu? Dia didefine di file app/Http/Kernel.php
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Dia memanggil file home.blade.php
        return view('home');

        // Seumpama ingin mengirim data bisa pakai seperti ini
        // return view('home', [$data1, $data2])

        // Seumpama file viewmu berada didalam folder1 dan masih harus masuk ke folder2
        // return view('folder1.folder2.home');
    }
}
