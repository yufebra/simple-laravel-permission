<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminAccessOnlyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Jika kondisi if terpenuhi, maka Middleware akan membolehkan user melanjutkan ke halaman yang ingin diakses
        if(auth()->user()->permission == 'usr') return $next($request);

        // Namun jika tidak, maka baris berikut akan dieksekusi. Baris berikut memberikan respon laravel error page sesuai code yg ditentukan
        return abort(403);

        // Atau kalian ingin diarahkan ke route tertentu juga bisa. Contoh ini mengarahkan user ke Halaman Dashboard lagi, bukan ke halaman yg ingin dituju
        return redirect()->route('home');
    }
}
